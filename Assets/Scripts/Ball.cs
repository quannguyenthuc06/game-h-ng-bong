using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    GameController m_gc;

    private void Start()
    {
        m_gc = FindObjectOfType<GameController>();   
    }

    // xay ra khi hai doi tuong va cham voi nhau
    private void OnCollisionEnter2D(Collision2D collision)
    {
        // Ball co va cham voi doi duong co tag la Player kho
        if (collision.gameObject.CompareTag("Player")) {
            m_gc.IncrementScore();
            Destroy(gameObject);
            Debug.Log("Pass");
        }
       
    }

    // Bat su kien xuyen qua
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("DeathZone"))
        {
            m_gc.SetGameoverState(true);
            Destroy(gameObject);
            Debug.Log("Game Over");
        }
    }
}
