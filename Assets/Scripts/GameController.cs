using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public GameObject ball;
    public float spawnTime;
    float m_spawnTime;

    int m_sore;
    bool m_isGameover;

    UIManager m_ui;
    
    // Start is called before the first frame update
    void Start()
    {
        m_spawnTime = 0;
        m_ui = FindObjectOfType<UIManager>();
        m_ui.SetScoreText("Score: " + m_sore);
    }

    // Update is called once per frame
    void Update()
    {
        if (m_isGameover)
        {
            m_spawnTime = 0;
            m_ui.ShowGameoverPanel(true);
            return;
        }
        m_spawnTime -= Time.deltaTime;
        if (m_spawnTime <= 0)
        {
            SpawnBall();
            m_spawnTime = spawnTime;
        }
    }

    public void SpawnBall()
    {
        Vector2 spawnPos = new Vector2(Random.Range(-15, 15), 8);
        if (ball)
        {
            Instantiate(ball, spawnPos, Quaternion.identity);
        }
    }

    public void SetScore(int value)
    {
        m_sore = value;
    }

    public int GetScore()
    {
        return m_sore;
    }

    public void IncrementScore()
    {
        m_sore++;
        m_ui.SetScoreText("Score: " + m_sore);
    }

    public bool IsGameOver()
    {
        return m_isGameover;
    }

    public void SetGameoverState (bool state)
    {
        m_isGameover = state;
    }

    public void Replay()
    {
        m_sore = 0;
        m_isGameover = false;
        m_ui.SetScoreText("Score: " + m_sore);
        m_ui.ShowGameoverPanel(false);
    }
}
